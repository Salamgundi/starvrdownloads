# README #
### What is this repository for? ###
A mirror of the downloads from the StarVR project.

A way for beta testers to stay up-to-date

### How do I get set up? ###

#### IOS/MAC OS ####
---
We will have IPAs/DMGs available within the next few weeks for iPhone/Mac users. (With More in-depth instructions)

#### Android ####
--- 
- Download the latest APK from this repos downloads page, 

- Then transfer it (unless you downloaded it from this page onto your phone) to your and android via USB Debugging or other means.

- Click on it and then hit accept, and wait for install

##### USB Debbuging ######

- Go into settings
- About
- Tap on the serial number 5-7 times until says `You are now a developer!`
- Go to developer settings, toggle ADB debugging - or ADB debugging over USB
- Done!
- If you Have any issues HMU @ 'Avery@Wagar.cc'



#### Windows ####
--- 
- Download .ZIP file

- Extract to preferred location

- Launch the app.


### Contribution guidelines ###

- Write issues to [**MAIN REPO**] (bitbucket.org/Salamgundi/starvr/issues) **Not Here**